import jwt from "jsonwebtoken";

//this api generates the token that will be used by the user to pass through or login to the website

export const generateToken = (user) => {
  return jwt.sign(
    {
      _id: user.id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
    },
    process.env.JWT_SECRET || 'somethingsecret',
    {
      expiresIn: "30d",
    }
  );

};

//this api authenticates the user

export const isAuth = (req, res, next) => {
    const authorization = req.headers.authorization;
    if(authorization) {
        const token = authorization.slice(7, authorization.length); //Bearer xxxxxx
        jwt.verify(token, process.env.JWT_SECRET || 'somethingsecret', (err, decode) => {
            if(err) {
                res.status(401).send({message: 'Invalid Token'});
            } else {
                req.user = decode;
                next();
            }
        })
    } else {
        res.status(401).send({message: 'No Token'});
    }
}

//this is an API that will only authenticate the user if he is an admin or not
//this will protect the admin from other users to access data such as userlist, product lists, updating the products

export const isAdmin = (req, res, next) => {
    if(req.user && req.user.isAdmin) {
        next();
    } else {
        res.status(401).send({message: 'Invalid Admin Token'});
    }
}